package uk.ac.leedsBeckett

class WelcomeScreenController {
	
    def accountManagementService //dependency injection

    def index() { }
    
    def authenticate(Integer accountId, String pin) {
        def result = accountManagementService.checkPin(accountId, pin)
        if (result) {
            def balance = accountManagementService.getBalance(accountId)
            redirect(controller: 'account', action: 'index', params: [accountId: accountId, balance: balance])
        } else {
            flash.message = "Credentials not valid"
            render(view: 'index')
        }
        return
    }
}
