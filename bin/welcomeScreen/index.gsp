<html>
<head>
    <asset:stylesheet src="application.css"/>
</head>

<body>
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2>ATM</h2>
        </div>

        <div class="panel-body">
            <g:if test="${flash.message}">
                <div class="alert alert-danger" role="alert">${flash.message}</div>
            </g:if>
            <g:form class="form-inline" name="welcomeScreenForm"
                    url="[controller: 'welcomeScreen', action: 'authenticate']">
                <div class="form-group">
                    <label for="accountId">Account Id:</label>
                    <g:textField class="form-control" name="accountId" value="${accountId}"/>
                </div>

                <div class="form-group">
                    <label for="pin">PIN:</label>
                    <g:textField class="form-control" name="pin" value="${pin}"/>
                </div>
                <g:actionSubmit value="submit" action="authenticate" class="btn btn-primary"/>
            </g:form>
        </div>
    </div>
</div>
</body>
</html>
