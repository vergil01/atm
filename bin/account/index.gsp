<html>
<head>
    <asset:stylesheet src="application.css"/>
</head>

<body>
<div class="container-fluid">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2>Authentication Successful</h2>
        </div>

        <div class="panel-body">
            <g:if test="${flash.message}">
                <div class="alert ${flash.message.contains("Insufficient") ? 'alert-danger' : 'alert-success'}"
                     role="alert">${flash.message}</div>
            </g:if>
            <g:form class="form-inline" name="withdrawalForm" url="[controller: 'account', action: 'withdraw']">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p>Account Id: ${params.accountId}</p>
                        Balance: &pound;<g:formatNumber number="${params.balance}" type="number" maxFractionDigits="2"
                                                  format="#"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="withdrawalAmount">Amount to withdraw:</label>
                    <g:textField class="form-control" name="withdrawalAmount" value="${withdrawalAmount}"/>
                </div>
                <input type="hidden" name="accountId" value="${params.accountId}"/>
                <input type="hidden" name="balance" value="${params.balance}"/>
                <g:actionSubmit value="submit" action="withdraw" class="btn btn-primary"/>
            </g:form>
        </div>
    </div>
</div>
</body>
</html>
