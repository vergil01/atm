package uk.ac.leedsBeckett

class AccountController {

    def accountManagementService //dependency injection

    def index() { }
    
    def withdraw() {
        def amount = Double.valueOf(params.withdrawalAmount)
        def balance = Double.valueOf(params.balance)
        if (amount <= balance) {
            accountManagementService.withdraw(Integer.valueOf(params.accountId), amount)
            balance -= amount
            flash.message = "Transaction successful. Please take your cash."
        } else {
            flash.message = "Insufficient funds. Please try a different amount."
        }
        redirect(controller: 'account', action: 'index', params: [accountId: params.accountId, balance: balance])
    }
}
