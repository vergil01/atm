package uk.ac.leedsBeckett

import grails.test.mixin.TestFor
import spock.lang.Specification
import uk.ac.leedsBeckett.AccountManagementService

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(WelcomeScreenController)
class WelcomeScreenControllerSpec extends Specification {

    void "test the authentication"() {
        given:"a mock of the injected service"
        def mockAccountManagementService = Mock(AccountManagementService)
        controller.accountQueryService = mockAccountManagementService

        when: "the authenticate() method is called"
        controller.authenticate(999999, '1234')

        then: "the injected service's checkPin() method is invoked once and only once"
        1 * mockAccountManagementService.checkPin(_, _)
    }

}
