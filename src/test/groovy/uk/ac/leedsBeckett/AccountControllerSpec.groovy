package uk.ac.leedsBeckett

import grails.test.mixin.TestFor
import spock.lang.Specification
import uk.ac.leedsBeckett.AccountManagementService

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(AccountController)
class AccountControllerSpec extends Specification {

    void "test the withdrawal function"() {
        given:"a mock of the injected service"
        def mockAccountManagementService = Mock(AccountManagementService)
        controller.accountManagementService = mockAccountManagementService

        and: "the required parameters"
        params.put('withdrawalAmount', 50)
        params.put('accountId', 1)
        params.put('balance', 100)

        when: "the withdraw() method is called"
        controller.withdraw()

        then: "the injected service's withdraw() method is invoked once and only once"
        1 * mockAccountManagementService.withdraw(_, _)
    }
}
